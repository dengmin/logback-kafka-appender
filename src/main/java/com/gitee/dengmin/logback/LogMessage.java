package com.gitee.dengmin.logback;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author dengmin
 * @Created 2020/6/9 下午4:34
 */
public class LogMessage implements Serializable {
    private static final long serialVersionUID = 1L;
    private String appName;
    private String method;
    private String  level;
    private String className;
    private int line;
    private String message;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.S",timezone="GMT+8")
    private Date dateTime;
    private long timestamp;
    private String traceId;

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public String getMessage() {
        return message;
    }

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public void setMessage(String message) {

        this.message = message;
    }
}
